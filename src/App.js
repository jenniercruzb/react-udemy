import React, {useState, useEffect, Component} from 'react';
import logo from './logo.svg';
import './App.css';
const axios = require('axios').default;

class App extends Component{

  constructor(){
    super();
    this.state = { datos: [] };
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/todos')
    .then((response)=>{

      this.setState({datos: response.data});

    });
  }

  contador = () => {

    //console.log(this.state);

    const objeto = {
      userId: 11,
      id: 11,
      title: 'prueba',
      completed: true
    }

    axios.post('https://jsonplaceholder.typicode.com/todos', objeto)
    .then((response)=>{
      console.log(response);
    }).catch(err=>console.log(err))
  }
  render(){
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          
          <div>
            <button onClick={this.contador}> CHANGE </button>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
